from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required

# Create your views here.

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)



def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)



@login_required
def create_recipe(request):
    if request.method == "POST":
        #Use the form to validate the values and save them to the db
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            #If all goes well, we can redirect the browser to another page and leave the function
            return redirect("recipe_list")
    else:
        #Create an instance of the Django model fron class
        form = RecipeForm()
        
    #put the form in the context
    context = {
        "form": form,
    }
    #render the Html template with the form
    return render(request, "recipes/create.html", context)



def edit_recipe(request, id):
    post = get_object_or_404(Recipe, id=id)
    
    if request.method == "POST":
    
        form = RecipeForm(request.POST, instance=post) 
        if form.is_valid():
            form.save()
            
            return redirect("recipe_list")
    else:
        form = RecipeForm(instance=post)
        
    context = {
        "post": post,
        "form": form,
    }
    return render(request, "recipes/edit.html", context)

